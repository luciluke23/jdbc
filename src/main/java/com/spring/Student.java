package com.spring;

public class Student {

	private int sid;
	private String name;
	private String add;

	public void setSid(int sid) {
		this.sid = sid;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAdd(String add) {
		this.add = add;
	}

	public int getSid() {
		return sid;
	}

	public String getName() {
		return name;
	}

	public String getAdd() {
		return add;
	}

}
